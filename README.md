# Community Documentation site based on Sphinx using ReadTheDocs Theme

## Requirements

**It is strongly recommended that you use a tool like [pyenv](https://github.com/pyenv/pyenv) (for Mac/Linux users) or [pyenv-win](https://github.com/pyenv-win/pyenv-win#installation) to manage your python versions.**

1. [pipenv](https://pipenv.readthedocs.io)
2. Python 3.7.x


## The documentation structure

1. All docs are to off of `docs/`
2. The path for a project should be `docs/projects/projectname` (e.g., `docs/projects/toolkit`)

## Building the docs

* On Linux, you can test your code by running `test-docs.sh`
* On Windows you can test your code using `make.bat`
