
Data Manager
============

Introduction
------------

The **Data Manager** is one of the roles in LibreHealth Toolkit (LH Toolkit). This document explains how a **Data Manager** can create new patients, edit patient information, use the ``Dictionary`` and perform administrative functions. The **Data Manager** has all the permissions of a **Data Assistant** plus the following permissions:

.. list-table::
   :header-rows: 1

   * - Manage
     - Delete
     - View
     - Add
     - Edit
     - Get
   * - Alerts
     - Cohorts
     - Graphs section
     - Observations
     - Encounters
     - Patient Cohorts
   * - Locations
     - Observations
     - Administrator functions
     -
     - Observations
     - Orders
   * -
     - Report Objects
     - Data Entry Statistics
     -
     -
     -
   * -
     -
     - Orders
     -
     -
     -
   * -
     -
     - Patient Cohorts
     -
     -
     -


Procedure
---------

Logging in as a Data Manager
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You need to be logged in as a **Data Manager** to proceed with this tutorial.

.. image:: images/logged-in-as-data-manager.png
   :target: images/logged-in-as-data-manager.png
   :alt:


Available operations
^^^^^^^^^^^^^^^^^^^^

As a **Data Manager**, you will see the dashboard with a clickable buttons ``Home``\ , ``Find/Create Patient``\ , ``Dictionary`` and ``Administration``.

Find/Create Patient
~~~~~~~~~~~~~~~~~~~

``Find/Create Patient`` will redirect you to the form where you can either search a patient or create a new one.


.. image:: images/find-create-patient.png
   :alt: find or create a patient


When searching a patient, autocomplete suggests possible matches. If there is a match, click on the name of this patient and you will be redirected to the corresponding dashboard.


.. image:: images/patient-search.png
   :alt: patient search


Here you can edit information about this patient. There are tqo tabs on the dashboard: ``Overview`` and ``Demographics``. You can add new relationships on the ``Overview`` tab. Click ``Add a new relationship`` to do this.


.. image:: images/patient-information.png
   :alt: patient information


Fill in the form in the popup and click ``Save``.

.. image:: images/adding-new-relationship.png
   :alt: adding new relationship


You will see this new relationship on the dashboard.

.. image:: images/relationship-added.png
   :alt: relationship added


Next tab is ``Demographics`` tab. Here two forms to edit patient's info are accessible.

.. image:: images/demographics.png
   :alt: demographics


A long form is similar to the form you fill in when creating a patient. A a short form is shown below:

.. image:: images/edit-patient.png
   :alt: edit a patient


Make necessary changes here and save them.

If you want to create a patient, navigate to the ``Find/Create Patient`` by clicking a corresponding link on the dashboard. Fill in the lower part of the form and click ``Create Person``.


.. image:: images/creating-new-patient.png
   :alt: creating new patient


You will be asked to enter information about this person. Click ``Save`` when you are ready.


.. image:: images/new-patient-info.png
   :alt: new patient info


Dictionary
~~~~~~~~~~

Click ``Dictionary`` on the dashboard to download a .csv file with medical terms for reference or to search for a term.

.. image:: images/dictionary.png
   :alt: dictionary


Administration
~~~~~~~~~~~~~~

``Administration`` tab on the dashboard allows performing various administrative tasks.

.. image:: images/administration-tab.png
   :alt: administration dashboard


Users
"""""

Click ``Manage Alerts`` on the ``Administration`` dashboard. To create a new alert, click ``Add Alert`` button.

.. image:: images/add-alert.png
   :alt: adding alert


You will be invited to fill in information on the alert and save it.

.. image:: images/alert-form.png
   :alt: alert form


All created alerts that are not expired are displayed in the ``Alerts`` table. Select alerts to expire and click ``Expire Selected Alerts``. They will be no longer shown in the table.

.. image:: images/expire-alert.png
   :alt: expire alert


Patients
""""""""

``Manage Patients`` function is similar to the ``Find/Create Patient`` link on the dashboard and is described above.
You can either look for an exisiting patient or create a new one.


.. image:: images/manage-patients.png
   :alt: manage patients


``Find Patients to Merge``  allows you to merge two patient cards into one.
You need to select criteria to find patients for merge and mark those of the found patients whose information you want to merge.

.. image:: images/selecting-patients-to-merge.png
   :alt: selecting patients to merge


When choosing this option, you will be asked which information should stay in there are discrepancies.

.. image:: images/merging-process.png
   :alt: merging process


Click ``Merge Patients`` ath the bottom of the form to apply changes.

Encounters
""""""""""

A **Data Manager** can view a search form for encounters but has insufficient permissions to use it.

.. image:: images/encounters.png
   :alt: encounters


Locations
"""""""""

A **Data Manager** can view a search form for location management but has insufficient permissions to use it.

.. image:: images/locations.png
   :alt: locations


Observations
""""""""""""

Find a patient you want to add an observation for. You can search either by ``Person and Concept`` or by ``Encounter``

.. image:: images/observations-dashboard.png
   :alt: observations dashboard


Fill in the form and save it when you are ready.

.. image:: images/filling-in-observation-info.png
   :alt: filling in observation information


Programs
""""""""

Choose ``Manage Programs``. You will see a list of available programs.
You can edit existing programs by clicking on the program link.

.. image:: images/selecting-program.png
   :alt: selecting a program


Make necessary changes in the next window and save them.

.. image:: images/saving-program-changes.png
   :alt: saving program changes


Click ``Add a new program`` link on the ``Programs`` dashboard to create a new program. You will be invited to fill in the same form as when editing an existing program. The new program will be shown in the table of programs once you save it.

If you need to delete a program or several programs, mark corresponding lines and click ``Delete Selected Program(s)``

You can also ``Manage Triggered State Conversions`` by adding a new one or editing the existing state conversions.

.. image:: images/add-state-conversion.png
   :alt: adding state conversion


Fill in the form and ``Save`` it.

.. image:: images/save-conversion.png
   :alt: saving state conversion


Concepts
""""""""

``Concepts`` functions are the same as for the ``Dictionary`` link on the main dashboard and is described above.

Maintenance
"""""""""""

Fill in the form to view quick reports and click ``Generate Report`` to see it on the screen.

.. image:: images/creating-quick-report.png
   :alt: creating a quick report


The report look like this:

.. image:: images/report.png
   :alt: report


Open Web Apps Module
""""""""""""""""""""

A **Data Manager** does not have sufficient privileges to use any of these functions.

Summary
-------

A **Data Manager** can add patients, relationships, edit this information and use the Dictionary.
