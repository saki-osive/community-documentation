
Clinician: Managing Patients and Visits
=======================================

Introduction
------------

The **Clinician** is one of the roles in LibreHealth Toolkit (LH Toolkit). This document explains how a **Clinician** can search for patients, view patient information and work with the ``Dictionary`` of medical terms. The full list of permissions for a **Clinician** can be found in the following table:

.. list-table::
   :header-rows: 1

   * - View
     - Get
   * - Allergies
     - Allergies
   * - Problem List
     - Concepts
   * - Concepts
     - Encounters
   * - Encounters
     - Observations
   * - Observations
     - Patient Identifiers
   * - Patient Identifiers
     - Patients
   * - Patients
     - People
   * - People
     - Problems
   * - Problems
     - Providers


Procedure
---------

Logging in
^^^^^^^^^^

To use the LibreHealth Toolkit with privileges of a **Clinician**, a user should be logged in into an account with these privileges.

.. image:: images/logged-in-as-clinician.png
   :alt: dashboard


Available operations
^^^^^^^^^^^^^^^^^^^^

As a **Clinician**, you will see the dashboard with a clickable button ``Find Patient``. It will redirect you to the form where you can either search a patient. Start typing patient's name in the box to see matches.


.. image:: images/searching-for-patient.png
   :alt: searching for a patient


You can view patient's information by clicking patient's name.


.. image:: images/patient-info.png
   :alt: patient info


Click ``Dictionary`` on the dashboard to download a .csv file with medical terms for reference or to search for a term.


.. image:: images/dictionary.png
   :alt: dictionary


Summary
-------

A **Clinician** can view patients' information and use a Dictionary of medical concepts.
