
Anonymous and Authenticated Users
=================================

Introduction
------------

Anonymous and Authenticated roles have different privileges but in the current version there are no differences in what users with these roles can do. Therefore, Anonymous and Authenticated user roles are described in one document.

Anonymous users can only view Navigation menu.

The list of privileges for Authenticated users is shown in the table below:

.. list-table::
   :header-rows: 1

   * - View
     - Get
   * - Relationships
     - Concept Classes
   * - Concept Datatypes
     - Concept Datatypes
   * - Concept Classes
     - Encounter Types
   * - Encounter Types
     - Field Types
   * - Field Types
     - Global Properties
   * - Global Prooerties
     - Identifier Types
   * - Identifier Types
     - Locations
   * - Locations
     - Order Types
   * - Order Types
     - Person Attribute Types
   * - Relationships
     - Privileges
   * - Relationship Types
     - Relationship Types
   * - Person Attribute Types
     - Relationships
   * - Roles
     - Roles


Procedure
---------

View and Permissions for Anonymous User
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An Anonymous user can log in and view Navigation menu.

.. image:: images/anonymous-user-dashboard.png
   :alt: anonymous user dashboard


View and Permissions for Authenticated User
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Dashboard for an Authenticated user looks the same as for an Anonymous user. For now, there are no clickable buttons apart from ``Home`` which leads you to the same page where you land after logging in.


.. image:: images/authenticated-user-dashboard.png
   :alt: authenticated user dashboard


Summary
-------

Both Anonymous and Authenticated user have limited privileges. These roles are not different in the current release of the application.
