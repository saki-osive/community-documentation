
System Developer: Creating Users and Appointing Roles
=========================================================

Introduction
------------

LibreHealth Toolkit (LH Toolkit) is a complex system that allows users to have different roles each of them having different levels of permissions (privileges). This document explains how the System Administrator can create new users, assign roles or change existing users’ roles.

.. note::

  This document assumes you are using the `Legacy UI Module`_. In order to gain access to most of user interfaces, you will need to be running the `Legacy UI Module`_. If using our `Docker image <https://gitlab.com/librehealth/toolkit/lh-toolkit-docker>`_, this is already included.


Procedure
---------

Who the System Developer is
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A System Developer (Super User) has the widest range of permissions or privileges.

When logged into the system, a System Developer can **create** new **users** and **appoint** them with the necessary **permissions**.

Creating a New Person
^^^^^^^^^^^^^^^^^^^^^

To do this, you need to be logged in as a System Developer.


.. image:: images/administrator-dashboard.png
   :alt: administrator dashboard


Click the ``Administration`` button.


.. image:: images/manage-users.png
   :alt: manage users


On the Administration screen, chose ``Manage Users``.


.. image:: images/add-user.png
   :alt: add user


On the User Management screen, choose ``Add User``.


.. image:: images/proceed-to-entering-user-info.png
   :alt: proceed-to-entering-user-info

On the Add User screen, create a new person by choosing ``Next``.

Assigning a Role
^^^^^^^^^^^^^^^^


.. image:: images/entering-user-info.png
   :alt: entering user info


Add user info including demographic info and user login information. Fields marked with an asterisk (*) are required.

You can appoint the user one or more roles. Currently, the following **roles** are available:


* **Anonymous**
* **Clinician**
* **Data Manager**
* **System Developer**
* **Authenticated**
* **Data Assistant**
* **Provider**

Advanced options
^^^^^^^^^^^^^^^^


.. image:: images/saving-user.png
   :alt: saving user


After checking the user role, you can click ``Advanced Options`` and set a secret question and answer for the user. The user can also set or change the secret question after logging in.

Changing existing User
^^^^^^^^^^^^^^^^^^^^^^

You can also change or add the role of the existing user.


.. image:: images/administrator-dashboard.png
   :alt: administrator dashboard


Click on the ``Administration`` link in the menu on top.


.. image:: images/manage-users.png
   :alt: manage users


Then click ``Manage Users``.


.. image:: images/choosing-user-to-edit.png
   :alt: choosing a user to edit


On the User Management screen, type name or choose role of the user you are looking for and click ``Search``. If there are any matches, you will see the list of users. ``System Id`` is a clickable link. Click on Id of the user you need to change. You will be redirected to the menu similar to the menu for creating user, where you can make necessary amendments.


.. image:: images/new-user-info.png
   :alt: more options


Similar to when adding a new user, you can save changes by clicking  ``Save User``.

Understanding roles
^^^^^^^^^^^^^^^^^^^

If you need more information on each role or need to change some of its privileges, click ``Administration`` link in the menu on top, than click ``Manage Roles``. You will see a table with the list of available roles. The list is a multiple choice list and you can select one or more roles to delete. Some roles have a lock icon on the left. These roles cannot be deleted.

You can click on every role to see detailed information about it. This information consists of the following parts:


* **Role**\ : a name of the role)
* **Description**\ : a short text that explains what is this role for)
* **Inherited roles**\ : a multiple choice list of roles whose permissions are inherited (incorporated) by this role
* **Privileges**\ : a multiple choice list of all possible privileges where privileges of the current role are checked and privileges from inherited roles are checked and greyed out. It is not possible to uncheck inherited privileges.
  Click ``Save Role`` button at the bottom of the page if you want any changes to be saved.


Dictionary
^^^^^^^^^^^^^^^^^^^^^

Click ``Dictionary`` on the dashboard or on the ``Administration`` dashboard to download a .csv file with medical terms for reference or to search for a term.

.. image:: images/dictionary.png
   :alt: dictionary


Alerts
^^^^^^^^^^^^^^^^^^^^^
Click ``Manage Alerts`` on the ``Administration`` dashboard. To create a new alert, click ``Add Alert`` button.

.. image:: images/add-alert.png
   :alt: adding alert


You will be invited to fill in information on the alert and save it.

.. image:: images/alert-form.png
   :alt: alert form


All created alerts that are not expired are displayed in the ``Alerts`` table. Select alerts to expire and click ``Expire Selected Alerts``. They will be no longer shown in the table.

.. image:: images/expire-alert.png
   :alt: expire alert


Summary
-------

A System Developer has the widest range of permissions or privileges and can assign roles and privileges to other users. We hope this guide has helped you to understand how to grant users **privileges** via **roles**.

.. _Legacy UI Module: https://dl.bintray.com/librehealth/lh-toolkit-legacyui/legacyui-1.4.0.omod
