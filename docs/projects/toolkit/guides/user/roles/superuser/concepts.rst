
System Developer: Concepts
==========================

Introduction
------------

System Developer can view concepts, manage concept drugs, proposed concepts, concept classes, datatypes, sources, stop word, attribute types and reference terms. These operations are described below.

Procedure
---------

Logging in as a System Developer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You need to be logged in as a System Developer to proceed with this tutorial. Navigate to ``Administration`` tab on the main dashboard on the top of the page and locate ``Concepts`` chapter.

.. image:: images/concepts.png
   :alt: concepts


Available operations
^^^^^^^^^^^^^^^^^^^^

View Concept Dictionary
~~~~~~~~~~~~~~~~~~~~~~~

``View Concept Didtionary`` allows searching the dictionary or downloading it in a .csv format.


.. image:: images/concept-dictionary.png
   :alt: concept dictionary


Manage Concept Drugs
~~~~~~~~~~~~~~~~~~~~

Click ``Manage Concept Drugs`` to see available concept drugs or add a new one.


.. image:: images/concept-drugs.png
   :alt: concept drugs


To add a new concept drug, click ``Add Concept Drug``. To edit information on the existing drug, click on the name of this drug. Forms to fill are similar. Click ``Save`` when you finish filling or editing the form.


.. image:: images/add-concept-drug-form.png
   :alt: add concept drug form


You can also retire or permanently delete a concept drug.

Manage Proposed Concepts
~~~~~~~~~~~~~~~~~~~~~~~~

``Manage Proposed Concepts`` allows proposing a new concept.

.. image:: images/proposed-concepts.png
   :alt: proposed concepts


Click ``Propose New Concept`` to submit a new concept and fill in the form.

.. image:: images/propose-concept.png
   :alt: proposed a concept


Manage Concept Classes
~~~~~~~~~~~~~~~~~~~~~~

``Manage Concept Classes`` allows adding, editing or deleting a concept class.

.. image:: images/manage-concept-classes.png
   :alt: manage concept classes


Click ``Add Concept Class`` to submit a new concept class and fill in the form.

.. image:: images/new-concept-class.png
   :alt: a new concept class


Click ``Save Concept Class`` to save the form.

Manage Concept Datatypes
~~~~~~~~~~~~~~~~~~~~~~~~

``Manage Concept Datatypes`` allows viewing existing datatypes.

.. image:: images/view-datatypes.png
   :alt: view datatypes


Manage Concept Sources
~~~~~~~~~~~~~~~~~~~~~~

``Manage Concept Sources`` allows editing existing or adding new concept sources.

.. image:: images/manage-concept-sources.png
   :alt: manage concept sources


Click ``Add New Concept Source`` and fill in the form.


.. image:: images/new-concept-source.png
   :alt: new concept source


Manage Concept Stop Word
~~~~~~~~~~~~~~~~~~~~~~~~

``Manage Concept Stop Word`` allows editing existing or adding new concept sources.


.. image:: images/concept-stop-word.png
   :alt: concept stop word


Here you can select and ``Delete Selected Concept Stop Word(s)``\ , edit them or add a new one by clicking ``Add New Concept Stop Word``.


.. image:: images/new-stop-word.png
   :alt: new stop word


Manage Concept Attribute Types
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``Manage Concept Attribute Types`` allows editing existing or adding new reference terms.


.. image:: images/manage-concept-attribute-types.png
   :alt: manage concept attribute types


Click ``Add Concept Attribute Type`` to add a new attribute type and fill in the form.


.. image:: images/new-concept-attribute-type.png
   :alt: new concept attribute type


Summary
-------

A System Developer can perform different operations to manage concepts in the system.
