
System Developer: Encounters
============================

Introduction
------------

System Developer can manage encounters, encounter types and encounter roles. These operations are described below.

Procedure
---------

Logging in as a System Developer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You need to be logged in as a System Developer to proceed with this tutorial. Navigate to ``Administration`` tab on the main dashboard on the top of the page and locate ``Encounters`` chapter.

.. image:: images/encounters-chapter.png
   :alt: encounters chapter


Available operations
^^^^^^^^^^^^^^^^^^^^

Manage Encounters
~~~~~~~~~~~~~~~~~

``Manage Encounters`` will redirect you to the form where you can either add an encounter or search for an encounter by entering encounter ID, patient ID or name.


.. image:: images/add-encounter.png
   :alt: add encounter


Click ``Add Encounter`` to access the form where you can fill in information about a new encounter.


.. image:: images/enter-encounter-info.png
   :alt: enter encounter info


Once you are ready, click ``Save Encounter``.

You can search for an existing encounter and edit it. Start typing encounter ID, patient ID or name in the search box and click on the encounter from the search result list to make changes.


.. image:: images/edit-encounter.png
   :alt: edit encounter


You can add an observation to the existing encounter by clicking ``Add Observation`` link in the edit form.


.. image:: images/add-observation.png
   :alt: add observation


You will be prompted to enter information on the new observation and save it.


.. image:: images/observation.png
   :alt: observation


This observation will appear on the encounter card.

Manage Encounter Type
~~~~~~~~~~~~~~~~~~~~~

Click ``Manage Encounter Types`` to see available encounter types or add a new one.
To add a new encounter type, click ``Add Encounter Type``.


.. image:: images/encounter-types.png
   :alt: encounter types


You can also edit any of the encounter types from the list bu clicking on the name of the type and making changes in the edit form.


.. image:: images/edit-encounter-type.png
   :alt: edit encounter type


You can also retire or delete this encounter type in the same form.

Manage Encounter Roles
~~~~~~~~~~~~~~~~~~~~~~

``Encounte Role Management`` allows editing or adding encounter roles with appropriate permissions.

.. image:: images/encounter-roles.png
   :alt: encounter roles


Click ``Add Encounter Role`` to add a new role or click one of the existing roles to make changes, retire or delete this role.

.. image:: images/edit-encounter-role.png
   :alt: edit encounter role


Summary
-------

A System Developer can perform different operations to manage encounters in the system.
