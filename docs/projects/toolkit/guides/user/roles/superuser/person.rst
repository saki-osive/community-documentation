
System Developer: Person
========================

Introduction
------------

System Developer can manage persons, personal information and attribute types. These operations are described below.

Procedure
---------

Logging in as a System Developer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You need to be logged in as a System Developer to proceed with this tutorial. Navigate to ``Administration`` tab on the main dashboard on the top of the page and locate ``Person`` chapter.

.. image:: images/person-chapter.png
   :alt: person chapter


Available operations
^^^^^^^^^^^^^^^^^^^^

Manage Persons
~~~~~~~~~~~~~~

``Manage Persons`` will redirect you to the form where you can either search a person or create a new one.


.. image:: images/create-person.png
   :alt: create a person


Click ``Create Person`` to access the form where you can fill in information about a new person.


.. image:: images/filling-in-personal-info.png
   :alt: filling in personal info


Once you are ready, click ``Create Person``. This will redirect you to the second form where you can add more information about this person. Add more information if needed and click ``Save Person`` at the bottom of the page.
You will see a notification at the top of the page that the person is saved and be redirected once more to the similar-looking form with one additional option - ``Delete Person``.


.. image:: images/delete-person.png
   :alt: delete a person


Relationship Type Management
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Click ``Relationship Type Management`` to see available relationship types or add a new one.
To add a new relationship type, click ``Add Relationship Type``.


.. image:: images/create-relationship-type.png
   :alt: create a relationship type


Fill in information on the new relationship and click ``Save Relationship Type``

.. image:: images/save-new-relationship.png
   :alt: save a new relationship


You will see this new type in the list of available types and can change or delete it by clicking on it and making necessary changes.

.. image:: images/delete-relationship-type.png
   :alt: delete relationship type


You can also ``Manage Relationship Type Views`` by selecting the corresponding dropdown ordering options.


.. image:: images/relationship-type-views.png
   :alt: relationship type views


Person Attribute Management
~~~~~~~~~~~~~~~~~~~~~~~~~~~

``Person Attribute Management`` allows viewing, changing and saving attribute types.
Select an attribute type from the list to be able to change it or fill in the form below the list to add a new one.

.. image:: images/person-attribute-management.png
   :alt: person attribute management



.. image:: images/changing-attribute-type.png
   :alt: changing attribute type


Summary
-------

A System Developer can perform different operations to manage persons in the system.
