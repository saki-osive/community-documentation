Running the project
-------------------

You can easily run the project locally by following the guide. You will need to set up an IDE and clone the project first.

.. toctree::
   :caption: Running the project
   :maxdepth: 1
   :hidden:

   running-the-project
   local-installation
