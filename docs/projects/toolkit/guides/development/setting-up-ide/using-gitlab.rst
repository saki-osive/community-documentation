Using GitLab
============

Forking the project
-------------------

To fork the project, login to your GitLab account and navigate to the `GitLab repository <https://gitlab.com/librehealth/toolkit/lh-toolkit>`_. Click the icon ``Fork`` and the project will be added to your account.

.. image:: images/forking.png
   :alt: forking


Cloning the project
-------------------

Navigate to your account and chose ``lh-toolkit`` project. You will see that it is forked and a link to the original repository.

.. image:: images/cloning.png
   :alt: cloning step 1


Click the button ``Clone`` and than a button to copy the link to the project.


.. image:: images/cloning2.png
   :alt: cloning step 2


Launch your IDE and chose the option ``Checkout from version control`` and than ``Git``.

.. image:: images/checking-out-from-git.png
   :alt: checking out from git


You will see a pop up window inviting to indicate the URL and Directory. Paste the URL you copied from the GitLab to the ``URL`` field and chose the directory where you want to clone the project to in the ``Directory`` field.


.. image:: images/saving-locally.png
   :alt: saving locally


The project will be cloned to your local directory and you will be asked if you want to open it. Click "Yes".

.. image:: images/opening-the-project.png
   :alt: opening the project


You will see the project in the IDE.

.. image:: images/open-project.png
   :alt: open project in the IDE
