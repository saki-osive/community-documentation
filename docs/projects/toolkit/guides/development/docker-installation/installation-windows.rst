Installing Docker on Windows 10
=================================

Docker is a computer program that performs operating-system-level virtualization, also known as "containerization". Docker allows you to install software quickly and without worrying about dependencies.
To use Docker images of software, you need to have Docker installed on your machine.

To check if you have Docker, run the terminal: click the "Start >> Program Files >> Accessories >> Command Prompt" to open a Command Prompt session using just your mouse. Click the "Start" button and type "cmd." Right-click "Cmd," select "Run as Administrator" and click "Yes" to open Command Prompt with elevated privileges.

Type in the terminal:

.. code-block::

   docker -v

If you do not see the version as an output, you need to install Docker. Most modern computes can run Docker, if you have any doubts, please check the `official documentation <https://docs.docker.com/>`_.

Downloading and installing Docker
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please visit the official page of `Docker Desktop for Windows <https://docs.docker.com/docker-for-windows/install/>`_ and click the Download button.


.. image:: images/windows/docker-official-page.png
   :alt: docker official page


You will be redirected to Dockerhub. You need to register and login to download the installation file.


.. image:: images/windows/logging-in.png
   :alt: logging in


The registration process is very simple and straightforward. After registering, enter you login and password.


.. image:: images/windows/signing-in.png
   :alt: signing in


You will be redirected to Dockerhub. Now the download button will be enabled.


.. image:: images/windows/downloading-docker.png
   :alt: downloading docker


Navigate to the directory where you saved the installation file. Double click the installation file.


.. image:: images/windows/windows-installer.png
   :alt: windows installer



.. image:: images/windows/download-progress.png
   :alt: download progress


Accept the defaults.

.. image:: images/windows/configuration.png
   :alt: configuration


The installation will continue.

.. image:: images/windows/docker-installation-1.png
   :alt: docker installation step 1



.. image:: images/windows/docker-installation-2.png
   :alt: docker installation step 2


Open the terminal and type:

.. code-block::

   docker -v

You should see the similar output.


.. image:: images/windows/docker-version.png
   :alt: docker version


Navigate to the folder where you installed Docker and double click on the *Docker for Windows.exe* file.

.. image:: images/windows/docker-executable.png
   :alt: docker executable


Soon you will see a pop-up window. Enter your credentials.


.. image:: images/windows/logging-in-to-docker.png
   :alt: logging in to docker


Pulling the container image
^^^^^^^^^^^^^^^^^^^^^^^^^^^

To run containers pull the container image:

.. code-block::

   docker pull registry.gitlab.com/librehealth/toolkit/lh-toolkit-docker:latest

You should see the similar console output:


.. image:: images/windows/pulling-container-image.png
   :alt: pulling container image


Navigate to the directory where you cloned this project. Depending on how you want to interact with the container, run it in foreground or as a daemon.

To run the container in the foreground:

.. code-block::

   docker-compose -f docker-compose.dev.yml up


.. image:: images/windows/running-in-foreground.png
   :alt: running in foreground


You may see a pop-up asking whether you want to share drive. Click *Share it* to continue.


.. image:: images/windows/share-drive.png
   :alt: share drive


MySQL will be started first and then LH Toolkit will be started on the containers.
When you are done using LH Toolkit you can press ``Ctrl+C`` to stop the container. Do not do this unless you want to stop Docker.

.. image:: images/windows/stopping-container.png
   :alt: stopping container


To run the container in the background:

.. code-block::

   docker-compose -f docker-compose.dev.yml up -d


.. image:: images/windows/running-as-daemon.png
   :alt: running as daemon


Using LH Toolkit
----------------

To start using LH Toolkit, point your browser to `localhost:8080/lh-toolkit <localhost:8080/lh-toolkit>`_.
The following are the authentication information:


* **User**\ : admin
* **Pass**\ : Admin123


.. image:: images/windows/login-screen.png
   :alt: login screen


Bringing container down
-----------------------

To bring the container down and to free space on your machine run:

.. code-block::

   docker-compose down


.. image:: images/windows/bringing-container-down.png
   :alt: bringing container down


Troubleshooting
---------------

When you are pulling the container image, the directory you are in does not matter.
However, if you try to run this docker image from outside of this project directory, you will get the following error:


.. image:: images/windows/running-image-from-wrong-dir.png
   :alt: running image from wrong directory


Navigate to the project directory and enter the command again.

If you try to pull an image before starting the Docker daemon, you will see an error:


.. image:: images/windows/docker-not-started-error.png
   :alt: docker not started error


Navigate to the folder where you installed Docker and double click on the *Docker for Windows.exe* file.

You might see the following warning on the older systems:

.. image:: images/windows/windows-deprecated.png
   :alt: windows deprecated


Ignore it or update your system.
