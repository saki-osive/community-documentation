#!/usr/bin/env bash
set -eux
pip install -U pip ; \
pip install -U pipenv ; \
pipenv install ; \
make livehtml
